# Prime Number Sieve
# https://www.nostarch.com/crackingcodes

import math, random

def is_prime_trial_division(num):
    # Returns True if num is a prime number, otherwise False.

    # Uses the trial division algorithm for testing primality.

    # All numbers less than 2 are not prime:
    if num < 2:
        return False
    
    # See if num is divisible by any number up to the square root of num:
    for i in range(2, int(math.sqrt(num)) + 1):
        if num % i == 0:
            return False
    return True

def prime_sieve(size):
    # Returns a list of prime numbers calculated using the Sieve of
    # Eratosthenes algorithm.

    sieve = [True] * size
    sieve[0] = False # Zero and one are not prime numbers
    sieve[1] = False

    # Create the sieve:
    for i in range(2, int(math.sqrt(size)) + 1):
        pointer = i * 2
        while pointer  < size:
            sieve[pointer] = False
            pointer += i

    # Compile the list of primes:
    primes = []
    for i in range(size):
        if sieve[i] == True:
            primes.append(i)
    
    return primes

def rabin_miller(num):
    # Returns True if num is a prime number.
    if num == 2:
        return True

    if num < 2 or num % 2 == 0:
        return False # Rabin-Miller doesn't work on even integers

    if num == 3:
        return True

    s = num - 1
    t = 0
    while s % 2 == 0:
        # Keep halving s until it is odd (and use t to count how many times
        # we havle s):
        s = s // 2
        t += 1

    for trials in range(5): # Try to falsify num's primality 5 times
        a = random.randrange(2, num - 1)
        v = pow(a, s, num)
        if v != 1: # This test does not apply if v is 1
            i = 0
            while v != (num - 1):
                if i == t - 1:
                    return False # Definitely compsite
                else:
                    i = i + 1
                    v = (v ** 2) % num
    
    return True # Probably prime
        
# Most of the time we can quickly determine if num is not prime by dividing
# by the first few dozen prime numbers.  This is quicker than rabin_miller()
# but in and of itself not sufficient.
LOW_PRIMES = prime_sieve(100)

def is_prime(num):
    # Return True if num is a prime number.  This function does a quicker
    # prime number check before calling rabin_miller()
    if num < 2:
        return False

    # see if any of the low prime numbers can divide num:
    for prime in LOW_PRIMES:
        if num % prime == 0:
            return False

    # If all else fails, call rabin_miller() to determine if num is prime:
    return rabin_miller(num)

if __name__ == '__main__':
    #print(prime_sieve(int(math.sqrt(2 ** 20))))
    #print(rabin_miller(2))
    #print(rabin_miller(3))
    #print(rabin_miller(2 ** 30))
    #print(rabin_miller(2 ** 64))
    #print(rabin_miller(1048573))
    #print(rabin_miller(1073741789))
    #print(rabin_miller(2 ** 2048))
    print(is_prime(2 ** 2048))